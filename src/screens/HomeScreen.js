/*******
 * Generated by nemo
 * on 3/12/20
 * version
 ********/
import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {formatRelative} from 'date-fns';
import {vi} from 'date-fns/locale';
import {useNavigation} from '@react-navigation/native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Footer,
  Text,
  Left,
  Right,
  Thumbnail,
  Button,
  Icon,
} from 'native-base';
import BookService from '../services/BookService';
import Theme from '../constants/Theme';
import Routes from '../constants/Routes';
const HomeScreen = () => {
  const navigation = useNavigation();
  const {container, bookItem, bookItemText} = styles;
  const [books, setBooks] = useState([]);
  let x = 0;
  useEffect(() => {
    const getBooks = async () => {
      x++;
      let lstBook = await BookService.getBooks();
      console.log({x});
      setBooks(lstBook);
    };
    getBooks();
  }, []);
  const _renderRatingItem = rate => {
    let active = rate;
    let notActive = 5 - rate;
    let lstStar = [];
    for (let i = 0; i < active; i++) {
      lstStar.push(
        <Icon
          key={i.toString()}
          name="md-star"
          style={{
            fontSize: 15,
            color: Theme.colors.primary,
          }}
        />,
      );
    }
    for (let i = 0; i < notActive; i++) {
      lstStar.push(
        <Icon
          key={(i + active).toString()}
          name="md-star-outline"
          style={{
            fontSize: 15,
          }}
        />,
      );
    }
    return <View style={{flexDirection: 'row'}}>{lstStar}</View>;
  };
  const _renderBookItem = item => {
    let totalRating = 0;

    if (item.ratings.length > 1) {
      totalRating = item.ratings.reduce((sum, item) => {
        sum + item.rate;
      }, 0);
    }
    let rateItem = Math.floor(totalRating / 5);
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate(Routes.BOOK_DETAIL, {bookID: item.id_gg_driver})
        }>
        <Card>
          <CardItem>
            <Body>
              <Text uppercase style={{color: Theme.colors.textColor}}>
                {item.title}
              </Text>
              <Text note style={{color: Theme.colors.darkLight}}>
                {item.category.name}
              </Text>
            </Body>
          </CardItem>
          <CardItem cardBody style={{backgroundColor: Theme.colors.background}}>
            <Image
              source={{uri: item.thumbnail}}
              style={{height: 150, width: null, flex: 1, resizeMode: 'contain'}}
            />
          </CardItem>
          <CardItem>
            <Left>
              <Body transparent>
                <Icon
                  style={{fontSize: 15, color: Theme.colors.primary}}
                  name="ios-eye"
                />
                <Text>{item.views}</Text>
              </Body>
            </Left>
            <Right>
              {_renderRatingItem(4)}
              <Text note>
                {formatRelative(new Date(item.created_at), new Date(), {
                  locale: vi,
                })}
              </Text>
            </Right>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <Header style={{alignItems: 'center', backgroundColor: 'transparent'}} r>
        <Text>SachOnline</Text>
      </Header>
      <FlatList
        data={books}
        renderItem={({item}) => _renderBookItem(item)}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        style={{}}
      />
    </Container>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
  },
  bookItem: {
    paddingTop: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  bookItemText: {
    justifyContent: 'space-between',
  },
});
export default HomeScreen;
